# README 
1. Start EAP server as ./standalone.sh (https://yadi.sk/d/pupbD-c-HI46Uw);
2. Database file is already in;
3. deploy EAR (already in deployments folder);
4. go to http://localhost:8080/journal;
5. enjoy.

# Workflow
* go to 3 journals and create one part of all types;
* go to car journal and build car from parts of point up;
* repeat much time you want);
* leave me feedback, i am still waiting.

### Some cases
* use any journals to view, sort, create, edit and delete items;
* use car journal to same operations with drop-down handbooks with stuff content contols;
* "+" button in grid or ENTER key to submit new item from last row of grid;
* use "-" button of grid to drop item, get exception about part is in use or not; 
* fabric IDs generates in runtime as random UUID.