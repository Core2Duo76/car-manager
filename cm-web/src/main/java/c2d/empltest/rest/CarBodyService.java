package c2d.empltest.rest;

import c2d.empl.BaseManager;
import c2d.empl.entity.CarBody;
import c2d.empl.man.CarBodyManager;
import c2d.empltest.rest.pojo.Request;
import c2d.empltest.rest.pojo.Response;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Stateless
@Path("/body")
@Produces(MediaType.APPLICATION_JSON)
public class CarBodyService extends BasePartService<CarBody> {

    @EJB
    CarBodyManager cbm;

    @Override
    public BaseManager<CarBody> getManager() {
        return cbm;
    }

    @POST
    @Path("/list")
    public Response<List<CarBody>> getItems(Request<CarBody> request) throws Exception {
        return new Response<>(getFilteredList(request));
    }

    @POST
    @Path("/create")
    public Response<CarBody> create(Request<CarBody> req) {
        CarBody body = req.getObject();
        try {
            return new Response<CarBody>().setResult(cbm.create(body));
        } catch (Exception ex) {
            ex.printStackTrace();
            return new Response<CarBody>().setError(ex.getMessage());
        }
    }

    @POST
    @Path("/update")
    public Response<CarBody> update(Request<CarBody> request) throws Exception {
        return updateAndGet(request);
    }

    @GET
    @Path("/delete/{id}")
    public Response<String> delete(@PathParam("id") long id) {
        Response<String> result = new Response<>();
        try {
            return result.setResult(cbm.remove(id));
        } catch (Exception ex) {
            return result.setError(ex.getMessage());
        }
    }

}
