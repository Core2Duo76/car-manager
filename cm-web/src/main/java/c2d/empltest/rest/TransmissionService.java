package c2d.empltest.rest;

import c2d.empl.BaseManager;
import c2d.empl.entity.Transmission;
import c2d.empl.man.TransmissionManager;
import c2d.empltest.rest.pojo.Request;
import c2d.empltest.rest.pojo.Response;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Stateless
@Path("/transmission")
@Produces(MediaType.APPLICATION_JSON)
public class TransmissionService extends BasePartService<Transmission> {

    @EJB
    TransmissionManager tm;

    @Override
    public BaseManager<Transmission> getManager() {
        return tm;
    }

    @Override
    @POST
    @Path("/list")
    public Response<List<Transmission>> getItems(Request<Transmission> request) throws Exception {
        return new Response<>(getFilteredList(request));
    }

    @POST
    @Path("/create")
    public Response<Transmission> create(Request<Transmission> req) {
        Transmission transmission = req.getObject();
        Response<Transmission> result = new Response<>();
        try {
            return result.setResult(tm.create(transmission));
        } catch (Exception ex) {
            ex.printStackTrace();
            return result.setError(ex.getMessage());
        }
    }

    @POST
    @Path("/update")
    public Response<Transmission> update(Request<Transmission> request) throws Exception {
        return updateAndGet(request);
    }

    @GET
    @Path("/delete/{id}")
    public Response<String> delete(@PathParam("id") long id) {
        Response<String> result = new Response<>();
        try {
            return result.setResult(tm.remove(id));
        } catch (Exception ex) {
            return result.setError(ex.getMessage());
        }
    }
}
