package c2d.empltest.rest;


import c2d.empl.EntityGenerator;
import c2d.empl.entity.Engine;
import c2d.empl.man.EngineManager;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/demo")
@Produces(MediaType.APPLICATION_JSON)
@Stateful //for post construct expression
public class DemoRestService {

    @EJB
    EngineManager carDao;

    @EJB
    EntityGenerator generator;

    @GET
    @Path("/engines")
    public List<Engine> allEngines() throws Exception {
        return carDao.getAll();
    }

}
