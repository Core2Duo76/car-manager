package c2d.empltest.rest.servlet;

import c2d.empltest.rest.grid.GridOptions;
import c2d.empltest.rest.grid.GridOptionsDispatcher;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class JournalServlet extends HttpServlet {

    private static final int PAGE_SIZE = 50;

    @EJB
    GridOptionsDispatcher optBuilder;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String enType = req.getParameter("type");
        if (enType == null) {
            resp.sendRedirect("/journal?type=car");
            return;
        } else {
            String sortDirection, sortField, sortExpression = "";
            String psPage = req.getParameter("page");
            String error = req.getParameter("error");
            String psPageSize = req.getParameter("pageSize");
            String query = req.getParameter("q");
            int pageSize = psPageSize != null ? Integer.parseInt(psPageSize) : PAGE_SIZE;
            int page = psPage != null ? Integer.parseInt(psPage) : 1;
            sortField = req.getParameter("sortBy");
            if (sortField == null || sortField.isEmpty()) {
                sortField = "";
            }
            sortDirection = req.getParameter("dir");
            if (sortDirection != null) {
                sortExpression = (sortField.isEmpty() ? "" : (sortField + "," + sortDirection));
            } else {
                sortDirection = "desc";
            }
            req.getSession().setAttribute("title", "Journal");
            req.getSession().setAttribute("entityType", enType);
            query = ("*".equals(query)) ? null : query;
            try {
                GridOptions opts = optBuilder.buildOptions(enType);
                List<Object> dataPage = opts.getData(sortExpression, page, pageSize, query);
                int allDataSize = opts.count();
                req.setAttribute("total", allDataSize);
                allDataSize = (allDataSize % PAGE_SIZE == 0) ? allDataSize / PAGE_SIZE : (allDataSize / PAGE_SIZE) + 1;
                req.setAttribute("pages", allDataSize);
                req.setAttribute("data", dataPage);
                req.setAttribute("fieldsOptions", opts.getFieldsOptions());
                sortDirection = "ascdesc".replace(sortDirection.toLowerCase(), "");
                req.getSession().setAttribute("sortDirection", sortDirection);
                req.getSession().setAttribute("newSerial", UUID.randomUUID().toString());
                req.getSession().setAttribute("error", error);
                req.getSession().setAttribute("q", query);
            } catch (Exception ex) {
                ex.printStackTrace();
                req.setAttribute("error", ex.getMessage());
            }
        }
        req.getRequestDispatcher("/journal.jsp").forward(req, resp);
    }

}
