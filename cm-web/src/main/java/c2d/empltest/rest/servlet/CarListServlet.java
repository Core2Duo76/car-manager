package c2d.empltest.rest.servlet;

import c2d.empl.entity.Car;
import c2d.empl.man.CarManager;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CarListServlet extends HttpServlet {

    private final int PAGE_SIZE = 5;

    @EJB
    CarManager cm;

    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        httpServletRequest.getSession().setAttribute("title", "Car list");
        String error = null;
        String sortDirection, sortField, sortExpression = "";
        String idToRemove = httpServletRequest.getParameter("delete");
        String psPage = httpServletRequest.getParameter("page");
        String psPageSize = httpServletRequest.getParameter("pageSize");
        Integer pageSize = psPageSize != null ? Integer.parseInt(psPageSize) : PAGE_SIZE;
        Integer page = psPage != null ? Integer.parseInt(psPage) : 1;
        if (idToRemove != null) {
            try {
                cm.remove(Long.parseLong(idToRemove));
            } catch (Exception ex) {
                httpServletRequest.getSession().setAttribute("error", ex.getMessage());
            }
            httpServletResponse.sendRedirect("/car-list");
            httpServletRequest.getRequestDispatcher("/car/car-list.jsp").forward(httpServletRequest, httpServletResponse);
            return;
        }

        sortField = httpServletRequest.getParameter("sortBy");
        if (sortField == null || sortField.isEmpty()) {
            sortField = "";
        }
        sortDirection = httpServletRequest.getParameter("dir");
        if (sortDirection != null) {
            sortExpression = (sortField.isEmpty() ? "" : (sortField + "," + sortDirection));
        } else {
            sortDirection = "desc";
        }

        List<Car> cars = new ArrayList<>();
        try {
            cars = cm.getSortedList(sortExpression, pageSize, page, null);
        } catch (Exception ex) {
            error = ex.getMessage();
            ex.printStackTrace();
        }
        // invert sorting
        sortDirection = "ascdesc".replace(sortDirection.toLowerCase(), "");

        httpServletRequest.getSession().setAttribute("total", cars.size());
        httpServletRequest.getSession().setAttribute("list", cars);
        httpServletRequest.getSession().setAttribute("error", error);
        httpServletRequest.getSession().setAttribute("sortDirection", sortDirection);
        httpServletRequest.getRequestDispatcher("/car/car-list.jsp").forward(httpServletRequest, httpServletResponse);
    }
}

