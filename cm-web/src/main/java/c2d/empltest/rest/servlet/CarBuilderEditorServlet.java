package c2d.empltest.rest.servlet;

import c2d.empl.entity.CarBody;
import c2d.empl.entity.Engine;
import c2d.empl.entity.Transmission;
import c2d.empl.man.CarBodyManager;
import c2d.empl.man.EngineManager;
import c2d.empl.man.TransmissionManager;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class CarBuilderEditorServlet extends HttpServlet {

    @EJB
    EngineManager em;
    @EJB
    TransmissionManager tm;
    @EJB
    CarBodyManager cbm;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("title", "Car builder");
        String prevError = req.getParameter("error");
        try {
            List<Engine> engines = em.getFreeItems();
            List<Transmission> transmissions = tm.getFreeItems();
            List<CarBody> bodies = cbm.getFreeItems();
            if (engines.isEmpty() || transmissions.isEmpty() || bodies.isEmpty()) {
                String partName;
                if (engines.isEmpty()) {
                    partName = "engine";
                } else if (transmissions.isEmpty()) {
                    partName = " transmission";
                } else {
                    partName = "body";
                }
                throw new Exception("Sorry, where is not enough " + partName + " in staff" +
                        "<br>Drop any " +
                        "<a href=\"/car-list\">car</a>? M?");
            }
            req.setAttribute("engineList", em.getFreeItems());
            req.setAttribute("trList", tm.getFreeItems());
            req.setAttribute("bodyList", cbm.getFreeItems());
            req.setAttribute("showControls", true);
            req.setAttribute("error", prevError);
        } catch (Exception ex) {
            ex.printStackTrace();
            req.setAttribute("error", ex.getMessage());
            req.setAttribute("showControls", false);
        }
        req.getRequestDispatcher("/car/car-builder.jsp").forward(req, resp);
    }
}
