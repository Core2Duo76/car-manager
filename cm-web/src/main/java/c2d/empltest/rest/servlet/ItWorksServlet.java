package c2d.empltest.rest.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ItWorksServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        String paramValue = httpServletRequest.getParameter("param");
        System.out.println("PARAM VALUE = " + paramValue);
        httpServletRequest.getSession().setAttribute("testAttr", paramValue);
        httpServletRequest.getRequestDispatcher("it-works.jsp").forward(httpServletRequest, httpServletResponse);
    }
}
