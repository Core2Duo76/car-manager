package c2d.empltest.rest.servlet;

import c2d.empl.entity.Car;
import c2d.empl.man.CarManager;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CarServlet extends HttpServlet {

    @EJB
    CarManager cm;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        request.getSession().setAttribute("title", "Car");
        String id = request.getParameter("id");
        if (id == null) {
            httpServletResponse.sendRedirect("/car-list");
            return;
        }
        Car car = cm.get(Long.parseLong(id));
        if (car == null) {
            request.setAttribute("error", "Car " + id + " not found");
        } else {
            request.setAttribute("car", car);
        }
        request.getRequestDispatcher("/car/car.jsp").forward(request, httpServletResponse);
    }
}
