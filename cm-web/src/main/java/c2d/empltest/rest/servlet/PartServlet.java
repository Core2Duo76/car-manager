package c2d.empltest.rest.servlet;

import c2d.empltest.rest.grid.GridOptions;
import c2d.empltest.rest.grid.GridOptionsDispatcher;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class PartServlet extends HttpServlet {

    @EJB
    GridOptionsDispatcher optBuilder;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String entityType = req.getParameter("entityType");
        String action = req.getParameter("action");
        if (entityType == null || action == null) {
            // redirect
            req.getSession().setAttribute("error", "Unknown entity type or action");
            resp.sendRedirect("/journal?type=engine");
            //req.getRequestDispatcher("/journal.jsp").forward(req, resp);
        } else {
            if ("delete".equalsIgnoreCase(action)) {
                try {
                    String enId = req.getParameter("id");
                    if (enId == null) {
                        throw new Exception("ID is missing");
                    }
                    GridOptions options = optBuilder.buildOptions(entityType);
                    req.getSession().setAttribute("error", options.remove(Long.parseLong(enId)));
                    req.getSession().setAttribute("type", entityType);
                    resp.sendRedirect("/journal?type=" + entityType);
                } catch (Exception ex) {
                    resp.sendRedirect("/journal?type=" + entityType + "&error=" + ex.getMessage());
                }
            } else if ("create".equalsIgnoreCase(action)) {
                final Map<String, String> entityParams = new HashMap<>();
                for (String paramStr : req.getQueryString().split("&")) {
                    String[] p = paramStr.split("=");
                    entityParams.put(p[0], p[1]);
                }
                try {
                    System.out.println(entityParams);
                    GridOptions options = optBuilder.buildOptions(entityType);
                    Object data = options.update(entityParams);
                    resp.sendRedirect("/journal?type=" + entityType);
                } catch (Exception ex) {
                    req.getSession().setAttribute("error", ex.getMessage());
                    ex.printStackTrace();
                }
                resp.sendRedirect("/journal?type=" + entityType);
            }

        }
    }

    @Override
    protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        final Map<String, String> entityParams = new HashMap<>();
        String entityType = httpServletRequest.getParameter("type");
        if (entityType == null) {
            httpServletResponse.sendRedirect("/journal?type=eingine");
            return;
        }
        Stream.of(httpServletRequest.getQueryString().split("&"))
                .map(paramStr -> paramStr.split("="))
                .map(arr -> entityParams.put(arr[0], arr[1]));
        try {
            httpServletResponse.sendRedirect("/journal?type=" + entityType);
        } catch (Exception ex) {
            httpServletResponse.sendRedirect("/journal?type=" + entityType);
        }
    }
}
