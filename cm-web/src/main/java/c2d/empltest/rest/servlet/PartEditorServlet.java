package c2d.empltest.rest.servlet;

import c2d.empltest.rest.grid.GridOptions;
import c2d.empltest.rest.grid.GridOptionsDispatcher;

import javax.ejb.EJB;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PartEditorServlet extends HttpServlet {

    @EJB
    GridOptionsDispatcher optsBuilder;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.getSession().setAttribute("title", "Editor");
        String type = req.getParameter("type");
        String sId = req.getParameter("id");
        if (type == null || sId == null) {
            resp.sendRedirect("/journal?type=car");
        } else {
            try {
                GridOptions opts = optsBuilder.buildOptions(type);
                Long id = Long.parseLong(sId);
                Object data = opts.load(id);
                req.getSession().setAttribute("data", data);
                req.getSession().setAttribute("fieldsOptions", opts.getFieldsOptions());
                req.getSession().setAttribute("entityType", type);
                req.getRequestDispatcher("/editor.jsp").forward(req, resp);
            } catch (Exception ex) {
                req.getSession().setAttribute("error", ex.getMessage());
                resp.sendRedirect("/journal?type=engine");
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Map<String, String> parameterMap = inlineMap(req.getParameterMap());
        String entityType = parameterMap.get("entityType");
        if (entityType == null) {
            resp.sendRedirect("/journal?type=engine");
        } else {
            try {
                GridOptions opts = optsBuilder.buildOptions(entityType);
                Object data = opts.update(parameterMap);
                resp.sendRedirect("/journal?type=" + entityType);
            } catch (Exception ex) {
                resp.sendRedirect("/journal?type=" + entityType + "&error=" + ex.getMessage());
            }
        }

    }

    private Map<String, String> inlineMap(final Map<String, String[]> in) {
        Map<String, String> result = new HashMap<>();
        for (String key : in.keySet()) {
            result.put(key, (in.get(key) != null && in.get(key).length > 0 ? in.get(key)[0] : null));
        }
        return result;
    }
}
