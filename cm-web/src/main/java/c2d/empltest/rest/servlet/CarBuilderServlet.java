package c2d.empltest.rest.servlet;

import c2d.empl.entity.Car;
import c2d.empl.ex.PartIsAlreadyUsedException;
import c2d.empl.ex.PartNotFoundException;
import c2d.empl.man.CarManager;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CarBuilderServlet extends HttpServlet {

    @EJB
    CarManager cm;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String engineId = request.getParameter("engineId");
        String trId = request.getParameter("transmissionId");
        String bodyId = request.getParameter("bodyId");
        String name = request.getParameter("name");
        name = name == null ? "Empty name" : name;
        if (engineId == null || trId == null || bodyId == null) {
            setError(request, response, "One of params is missing");
        } else {
            try {
                Car newCar = cm.build(Long.parseLong(engineId), Long.parseLong(trId), Long.parseLong(bodyId), name);
                request.getSession().setAttribute("id", newCar.getId());
                response.sendRedirect("/car?id=" + newCar.getId());
            } catch (PartNotFoundException | PartIsAlreadyUsedException ex) {
                ex.printStackTrace();
                setError(request, response, ex.getMessage());
            }
        }
    }

    private void setError(HttpServletRequest request, HttpServletResponse response, String text) throws ServletException, IOException {
        request.getSession().setAttribute("error", text);
        response.sendRedirect("/car-builder");
        request.getRequestDispatcher("/car/car-builder.jsp").forward(request, response);
    }
}
