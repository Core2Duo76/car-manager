package c2d.empltest.rest.grid;

import java.util.List;

public class GridColumn {

    public final String fieldName;
    public final String headerName;
    public final String type;
    public final List<Object> handBook;
    public final boolean readOnly;
    public final boolean forNew;

    public GridColumn(String fieldName, String headerName, String type, List<Object> hb, boolean readOnly, boolean fn) {
        this.fieldName = fieldName;
        this.headerName = headerName;
        this.type = type;
        this.handBook = hb;
        this.readOnly = readOnly;
        this.forNew = fn;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getHeaderName() {
        return headerName;
    }

    public String getType() {
        return type;
    }

    public List<Object> getHandBook() {
        return handBook;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public boolean isForNew() {
        return forNew;
    }

    @Override
    public String toString() {
        return "{" + fieldName + " = " + headerName + "}" + (handBook == null ? "" : handBook.toString());
    }
}
