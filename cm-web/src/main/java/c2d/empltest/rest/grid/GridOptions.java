package c2d.empltest.rest.grid;

import c2d.empl.BaseManager;
import c2d.empl.ex.PartIsAlreadyUsedException;
import c2d.empl.ex.PartNotFoundException;

import java.util.List;
import java.util.Map;

public class GridOptions<T> {

    private List<GridColumn> fieldsOptions;
    private BaseManager<T> manager;


    public GridOptions(BaseManager<T> manager, List<GridColumn> fields) {
        this.manager = manager;
        this.fieldsOptions = fields;
    }

    public String remove(Long id) throws Exception {
        return manager.remove(id);
    }

    public T load(Long id) {
        return manager.get(id);
    }


    public T update(Map<String, String> map) throws PartNotFoundException, PartIsAlreadyUsedException {
        return manager.update(manager.convert(map));
    }

    public List<GridColumn> getFieldsOptions() {
        return fieldsOptions;
    }

    public void setFieldsOptions(List<GridColumn> fieldsOptions) {
        this.fieldsOptions = fieldsOptions;
    }

    public List<T> getData(String sortExp, int page, int pageSize, String filter) throws Exception {
        return manager.getSortedList(sortExp, pageSize, page, filter);
    }

    public int count() throws Exception {
        return manager.count();
    }

    public void setManager(BaseManager<T> manager) {
        this.manager = manager;
    }
}
