package c2d.empltest.rest.grid;


import c2d.empl.entity.CarBodyType;
import c2d.empl.entity.EngineType;
import c2d.empl.entity.TransmissionType;
import c2d.empl.man.CarBodyManager;
import c2d.empl.man.CarManager;
import c2d.empl.man.EngineManager;
import c2d.empl.man.TransmissionManager;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Stateless
public class GridOptionsDispatcher {

    @Inject
    EngineManager em;
    @EJB
    CarManager cm;
    @EJB
    CarBodyManager cbm;
    @EJB
    TransmissionManager tm;

    public GridOptionsDispatcher() {
    }

    public GridOptions buildOptions(String typeName) throws Exception {
        List<GridColumn> options = new ArrayList<>();
        switch (typeName.toLowerCase().trim()) {
            case "engine": {
                options.add(new GridColumn("id", "ID", "hide", null, true, false));
                options.add(new GridColumn("type", "Type", "enum", Arrays.asList(EngineType.values()), false, true));
                options.add(new GridColumn("name", "Name", "text", Arrays.asList(EngineType.values()), false, true));
                options.add(new GridColumn("serial", "Serial", "text", null, true, true));
                options.add(new GridColumn("capacity", "Capacity", "number", null, false, true));
                options.add(new GridColumn("power", "Power", "float", null, false, true));
                return new GridOptions<>(em, options);
            }
            case "transmission": {
                options.add(new GridColumn("id", "ID", "hide", null, true, false));
                options.add(new GridColumn("typeId", "Type", "enum", Arrays.asList(TransmissionType.values()), false, true));
                options.add(new GridColumn("serial", "Serial", "text", null, true, true));
                return new GridOptions<>(tm, options);
            }
            case "body": {
                options.add(new GridColumn("id", "ID", "hide", null, true, false));
                options.add(new GridColumn("doors", "Doors", "number", null, false, true));
                options.add(new GridColumn("vin", "VIN", "text", null, true, true));
                options.add(new GridColumn("color", "Color", "text", null, false, true));
                options.add(new GridColumn("typeId", "Type", "enum", Arrays.asList(CarBodyType.values()), false, true));
                return new GridOptions<>(cbm, options);
            }
            case "car": {
                options.add(new GridColumn("id", "ID", "hide", null, true, false));
                options.add(new GridColumn("name", "Name", "text", null, false, true));
                options.add(new GridColumn("engine", "Engine", "handbook", (List) em.getFreeItems(), false, true));
                options.add(new GridColumn("body", "Body", "handbook", (List) cbm.getFreeItems(), false, true));
                options.add(new GridColumn("transmission", "Transmission", "handbook", (List) tm.getFreeItems(), false, true));
                return new GridOptions<>(cm, options);
            }
            default: {
                throw new Exception("Unknown journal type");
            }
        }
    }

}
