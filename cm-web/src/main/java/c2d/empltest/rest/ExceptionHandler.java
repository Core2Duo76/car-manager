package c2d.empltest.rest;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ExceptionHandler implements ExceptionMapper<Exception> {

    @Override
    public Response toResponse(Exception exception) {
        c2d.empltest.rest.pojo.Response<String> result = new c2d.empltest.rest.pojo.Response<>(exception);
        return Response.status(Response.Status.OK).entity(result).build();
    }
}
