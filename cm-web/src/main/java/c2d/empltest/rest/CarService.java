package c2d.empltest.rest;

import c2d.empl.BaseManager;
import c2d.empl.entity.Car;
import c2d.empl.ex.PartIsAlreadyUsedException;
import c2d.empl.ex.PartNotFoundException;
import c2d.empl.man.CarManager;
import c2d.empltest.rest.pojo.BuildByBodyRequest;
import c2d.empltest.rest.pojo.Request;
import c2d.empltest.rest.pojo.Response;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Stateless
@Path("/car")
public class CarService extends BasePartService<Car> {

    @EJB
    CarManager cm;

    @Override
    public BaseManager<Car> getManager() {
        return cm;
    }

    @GET
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<Car>> getItems(Request<Car> request) throws Exception {
        return new Response<>(getFilteredList(request));
    }

    @GET
    @Path("/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<String> delete(@PathParam("id") long id) {
        Response<String> response = new Response<>();
        try {
            return response.setResult(cm.remove(id));
        } catch (Exception ex) {
            return response.setError(ex.getMessage());
        }
    }

    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<Car> build(BuildByBodyRequest request) {
        if (!request.isValid()) {
            return new Response<Car>().setError("One or more fields are empty");
        }
        try {
            Car car = cm.build(request.getEngineId(), request.getTransmissionId(), request.getBodyId(), request.getName());
            return new Response<>(car);
        } catch (PartNotFoundException | PartIsAlreadyUsedException pnfe) {
            pnfe.printStackTrace();
            return new Response<>(pnfe);
        } catch (Exception ex) {
            ex.printStackTrace();
            return new Response<>();
        }
    }

    @Override
    public Response<Car> create(Request<Car> req) {
        return null;
    }
}
