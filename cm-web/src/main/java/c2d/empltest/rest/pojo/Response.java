package c2d.empltest.rest.pojo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "customer")
public class Response<T> {

    private String error;
    private boolean status = true;
    private T result;

    public Response() {
    }

    public Response(Exception ex) {
        this.error = ex.getMessage();
        this.status = false;
    }

    public Response(T result) {
        this.result = result;
    }

    @XmlElement
    public String getError() {
        return error;
    }

    public Response<T> setError(String error) {
        this.error = error;
        this.status = false;
        return this;
    }

    @XmlElement
    public boolean isStatus() {
        return status;
    }

    public Response<T> setStatus(boolean status) {
        this.status = status;
        return this;
    }

    @XmlElement
    public Object getResult() {
        return result;
    }

    public Response<T> setResult(T result) {
        this.result = result;
        return this;
    }
}
