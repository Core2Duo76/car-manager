package c2d.empltest.rest.pojo;

public class Request<T> {

    private T object;
    private Integer page = 1;
    private Integer pageSize = 10;
    private String filter;
    private String sortField, sortDirection;

    public Request() {
    }

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }

    public Integer getPage() {
        return page != null && page > 0 ? page : 1;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    public String getSortExpression() {
        return (sortField == null || sortField.isEmpty() ? "id" : sortField)
                + "," +
                (sortDirection == null || sortDirection.isEmpty() ? "asc" : sortDirection).toLowerCase();
    }
}
