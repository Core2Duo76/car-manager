package c2d.empltest.rest.pojo;

public class BuildByBodyRequest {

    private Long bodyId, transmissionId, engineId;
    private String name;
    /**
     * Find any object if not found;
     */
    private boolean findAny;

    public BuildByBodyRequest() {
    }

    public Long getBodyId() {
        return bodyId;
    }

    public void setBodyId(Long bodyId) {
        this.bodyId = bodyId;
    }

    public Long getTransmissionId() {
        return transmissionId;
    }

    public void setTransmissionId(Long transmissionId) {
        this.transmissionId = transmissionId;
    }

    public Long getEngineId() {
        return engineId;
    }

    public void setEngineId(Long engineId) {
        this.engineId = engineId;
    }

    public boolean isFindAny() {
        return findAny;
    }

    public void setFindAny(boolean findAny) {
        this.findAny = findAny;
    }

    public String getName() {
        return name == null || name.isEmpty() ? "Unnamed" : name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isValid() {
        return findAny || (transmissionId != null && bodyId != null && engineId != null);
    }
}
