package c2d.empltest.rest;

import c2d.empl.BaseManager;
import c2d.empl.entity.Engine;
import c2d.empl.man.EngineManager;
import c2d.empltest.rest.pojo.Request;
import c2d.empltest.rest.pojo.Response;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Stateless
@Path("/engine")
@Produces(MediaType.APPLICATION_JSON)
public class EngineService extends BasePartService<Engine> {

    @EJB
    EngineManager em;

    @Override
    public BaseManager<Engine> getManager() {
        return em;
    }

    @Override
    @POST
    @Path("/list")
    public Response<List<Engine>> getItems(Request<Engine> request) throws Exception {
        return new Response<>(getFilteredList(request));
    }

    @POST
    @Path("/create")
    public Response<Engine> create(Request<Engine> in) {
        Engine engine = in.getObject();
        Response<Engine> result = new Response<>();
        try {
            return result.setResult(em.create(engine));
        } catch (Exception ex) {
            ex.printStackTrace();
            return result.setError(ex.getMessage());
        }
    }

    @POST
    @Path("/update")
    public Response<Engine> update(Request<Engine> request) throws Exception {
        return updateAndGet(request);
    }

    @GET
    @Path("/delete/{id}")
    public Response<String> delete(@PathParam("id") long id) {
        Response<String> result = new Response<>();
        try {
            return result.setResult(em.remove(id));
        } catch (Exception ex) {
            return result.setError(ex.getMessage());
        }
    }


}
