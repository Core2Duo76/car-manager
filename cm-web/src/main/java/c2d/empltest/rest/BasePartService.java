package c2d.empltest.rest;

import c2d.empl.BaseManager;
import c2d.empltest.rest.pojo.Request;
import c2d.empltest.rest.pojo.Response;

import java.util.List;

public abstract class BasePartService<T> {

    public abstract Response<T> create(Request<T> req);

    public abstract Response<String> delete(long id);

    public T update(T object) {
        return getManager().update(object);
    }

    public abstract BaseManager<T> getManager();

    public abstract Response<List<T>> getItems(Request<T> request) throws Exception;

    protected List<T> getFilteredList(Request<T> request) throws Exception {
        return getManager().getSortedList(request.getSortExpression(),
                request.getPageSize(),
                request.getPage(),
                request.getFilter());
    }

    public Response<T> updateAndGet(Request<T> request) throws Exception {
        T entity = request.getObject();
        if (entity == null) {
            throw new Exception("Unknown entity");
        }
        return new Response<>(getManager().update(entity));
    }


}
