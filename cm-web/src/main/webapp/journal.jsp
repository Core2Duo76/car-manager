<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.ArrayList"%>
<html>
<head>
    <title>${title}</title>
</head>
<body>
    <jsp:include page="common-header.jsp" />
    <h3>${title} (${total} items)</h3><br>
    <h3>${error}</h3>

    <center>
    <pre>
    <a href="/journal?type=car"><b>Cars</b></a>   <a href="/journal?type=body">Bodies</a>    <a href="/journal?type=transmission">Transmissions</a>    <a href="/journal?type=engine">Engines</a>
    </pre>
    <table border="1">
        <tr>
        <form action="/journal?type=${entityType}" method="get">
            <input  type="text" name="type" value="${entityType}" hidden/>
            <td colspan=${fieldsOptions.size() + 1}><input name="q" type="text" placeholder="Search... (or *)" required/>
            <input type="submit" value="?"/></td>
        </tr>
        </form>
    <tr>
      <c:forEach items="${fieldsOptions}" var="col">
           <!-- header -->
           <td><p align="center"><a href="/journal?type=${entityType}&sortBy=${col['fieldName']}&dir=${sortDirection}">${col['headerName']}</a></p></td>
       </c:forEach>
       <td>Delete</td>
    </tr>

    <!-- data -->
    <c:forEach items="${data}" var="item">
    <tr>
    <c:forEach items="${fieldsOptions}" var="col">
        <c:set var="fName" value="${col['fieldName']}"/>
         <td><p align="center">
            <c:if test="${fName == 'id'}">
             <a href="/part-edit?type=${entityType}&id=${item[fName]}">${item[fName]}</a>
            </c:if>
            <c:if test="${fName != 'id'}">
             ${item[fName]}
            </c:if>
         </p></td>
       </c:forEach>
       <td><p align="center"><a href="/part?entityType=${entityType}&id=${item.id}&action=delete">-</a></p></td>
    </tr>
    </c:forEach>
    <!-- builder element -->

    <tr>
        <td colspan=${fieldsOptions.size() +1}><p align="center">CREATE NEW</p></td>
    </tr>
    <tr bgcolor="#40E0D0">
    <form action="/part" method="get">

    <input type="text" name="action" value="create" hidden/>
    <input type="text" name="entityType" value="${entityType}" hidden/>

        <c:forEach items="${fieldsOptions}" var="col">
            <c:set var="colType" value="${col.type}"/>
            <c:set var="colName" value="${col.fieldName}"/>
            <c:set var="colForNew" value="${col.forNew}"/>
            <c:set var="colReadOnly" value="${col.readOnly}"/>

            <c:if test="${colType == 'hide'}">
               <td><input type="submit" value="+" title="Create"></td>
            </c:if>

            <c:if test="${ (colType == 'handbook' || colType == 'enum') && colForNew}">
              <td><p align="center">
              <select name=${colName} required="true">
                <c:forEach items="${col.handBook}" var="hb">
                <c:set var="optValue" value="${colType == 'enum' ? hb : hb['id']}"/>
                    <option value="${optValue}">${hb}</option>
                </c:forEach>
              </select>
              </p>
              </td>
            </c:if>

            <c:if test="${colType == 'text'}">
                <c:if test="${colReadOnly}">
                    <td><p align="center"> to be generated </p></td>
                </c:if>
                <c:if test="${!colReadOnly}">
                    <td><p align="center">
                        <input name="${colName}" type="text" required="true"/>
                        </p>
                    </td>
                </c:if>
            </c:if>

            <c:if test="${colType == 'number'}">
              <td><p align="center">
                <input name="${colName}" type="number" required="true" min=0/>
                </p>
              </td>
            </c:if>

            <c:if test="${colType == 'float'}">
              <td><p align="center">
                <input name="${colName}" type="number" required="true" step="0.01" min=0/>
                </p>
              </td>
            </c:if>

        </c:forEach>
    </form>
    </tr>

    </table>
    <% ArrayList list = (ArrayList) request.getAttribute("data"); %>
    <% Integer pages = (int) request.getAttribute("pages"); %>
    <% for (int i = 1; i <= pages; i++){ %>
           <a href="/journal?type=${entityType}&page=<%= i%>">-page <%= i%>-</a>
       <% } %>
    </center>

</body>
</html>