<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.ArrayList"%>
<html>
<head>
    <title>${title}</title>
</head>
<body>
    <jsp:include page="common-header.jsp" />
    <h3>${title}</h3><br>
    <h3>${error}</h3>

    <center>
    <pre>
    <a href="/journal?type=body">Bodies</a>    <a href="/journal?type=transmission">Transmissions</a>    <a href="/journal?type=engine">Engines</a>
    </pre>
    <!-- Header -->
    <table border="1">
    <tr>
        <td colspan=${fieldsOptions.size() + 1}><p align="center">EDITOR</p></td>
    </tr>
    <tr>
      <c:forEach items="${fieldsOptions}" var="col">
           <!-- header -->
           <td><p align="center">${col['headerName']}</p></td>
       </c:forEach>
    </tr>

    <tr bgcolor="#40E0D0">
    <form action="/part-edit" method="post">

    <input type="text" name="entityType" value="${entityType}" hidden/>
    <input type="number" name="id" value="${data['id']}" hidden/>

        <c:forEach items="${fieldsOptions}" var="col">
            <c:set var="colType" value="${col.type}"/>
            <c:set var="colName" value="${col.fieldName}"/>
            <c:set var="colForNew" value="${col.forNew}"/>
            <c:set var="colReadOnly" value="${col.readOnly}"/>
            <c:set var="colValue" value="${data[colName]}"/>
            <c:if test="${colType == 'hide'}">
               <td><input type="submit" value="Save" title="Create"></td>
            </c:if>

            <c:if test="${(colType == 'enum' || colType == 'handbook') && colForNew}">
              <td><p align="center">
              <select name=${colName} required="true">
                <c:forEach items="${col.handBook}" var="hb">
                <c:set var="hbId" value="${colType == 'enum' ? hb : hb['id']}"/>
                    <option value="${hb}">${hb}</option>
                </c:forEach>
              </select>
              </p>
              </td>
            </c:if>

            <c:if test="${colType == 'text'}">
                <c:if test="${colReadOnly}">
                    <td><p align="center">${colValue}</p></td>
                </c:if>
                <c:if test="${!colReadOnly}">
                    <td><p align="center">
                        <input name="${colName}" type="text" required="true" value="${colValue}"/>
                        </p>
                    </td>
                </c:if>
            </c:if>
            <c:if test="${colType == 'number'}">
              <td><p align="center">
                <input name="${colName}" type="number" required="true" min=0 value="${colValue}"/>
                </p>
              </td>
            </c:if>

            <c:if test="${colType == 'float'}">
              <td><p align="center">
                <input name="${colName}" type="number" required="true" step="0.01" min=0 value="${colValue}"/>
                </p>
              </td>
            </c:if>

        </c:forEach>
    </form>
    </tr>

    </table>
    </center>
</body>
</html>