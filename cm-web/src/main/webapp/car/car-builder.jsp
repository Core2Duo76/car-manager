<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>${title}</title>
</head>
<body>
    <jsp:include page="../common-header.jsp"/>
    <h3>${title}</h3><br>
    <h3>${error}</h3>
    <form action="build" method="get">
        <c:if test="${showControls}">
        Name: <input type="text" required="true" name="name"/>
        <br>
        Engine:
        <select name="engineId" required="true">
            <c:forEach items="${engineList}" var="item">
                <option value="${item.id}">${item.type}/${item.capacity}/${item.power}</option>
            </c:forEach>
        </select>
        <br>
        Transmission:
        <select name="transmissionId" required="true">
            <c:forEach items="${trList}" var="item">
                <option value="${item.id}">${item.typeId}</option>
            </c:forEach>
        </select>
        <br>
        Body:
        <select name="bodyId" required="true">
            <c:forEach items="${bodyList}" var="item">
                <option value="${item.id}">${item.color}/${item.doors} d.</option>
            </c:forEach>
        </select>
        <input type="submit" value="Create"/>
        </c:if>
    </form>
    </center>
</body>
</html>