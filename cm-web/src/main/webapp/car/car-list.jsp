<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>${title}</title>
</head>
<body>
    <jsp:include page="../common-header.jsp" />
    <h3>${title} (${total} items)</h3><br>
    <h3>${error}</h3>
    <center>
    <table border="1">
      <tr>
        <td><a href="/car-list?sortBy=id&dir=${sortDirection}">ID</a></td>
        <td><a href="/car-list?sortBy=name&dir=${sortDirection}">NAME</td>
        <td><a href="/car-list?sortBy=body&dir=${sortDirection}">Color/doors</a></td>
        <td><a href="/car-list?sortBy=engine&dir=${sortDirection}">Engine/power/cap</a></td>
        <td><a href="/car-list?sortBy=transmission&dir=${sortDirection}">Transmission type</a></td>
      </tr>
      <c:forEach items="${list}" var="item">
      <tr>
        <td><a href="/car?id=${item.id}"><c:out value="${item.id}"/></a></td>
        <td><c:out value="${item.name}"/></td>
        <td title="vin ${item.body.vin}"><c:out value="${item.body.color}/${item.body.doors}"/></td>
        <td title="serial ${item.engine.serial}"><c:out value="${item.engine.type}/${item.engine.power}/${item.engine.capacity}"/></td>
        <td title="serial ${item.transmission.serial}"><c:out value="${item.transmission.typeId}"/></td>
      </tr>
      </c:forEach>
    </table>
    </center>
</body>
</html>