<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>${title}</title>
</head>
<body>
    <jsp:include page="../common-header.jsp" />
    <h3>${title}</h3><br>
    <center>
     ID: ${car.id}<br>
     <b>Body</b><br>
     Color: ${car.body.color}<br>
     Doors count: ${car.body.doors}<br>
     VIN: ${car.body.vin}<br>
     <b>Engine</b><br>
     Serial: ${car.engine.serial}<br>
     Capacity: ${car.engine.capacity}<br>
     Power: ${car.engine.power}<br>
     <b>Transmission</b><br>
     Serial: ${car.transmission.serial}<br>
     Type: ${car.transmission.typeId}<br>
     <br>
     <a href="/car-list?delete=${car.id}">Delete/Disassemble</a>
     <a href="/car-list">Back to list</a>
    </center>
</body>
</html>