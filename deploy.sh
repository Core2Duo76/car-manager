export WD=$(pwd);
cd $WD;
mvn clean install;
cd cm-ear/target;
cp *.ear ~/SERVERS/EAP-7.3.0/standalone/deployments/ -fr
cd $WD;
