package c2d.empl;

import c2d.empl.ex.PartIsAlreadyUsedException;
import c2d.empl.ex.PartNotFoundException;
import c2d.empl.ex.UnknownPartException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public abstract class BaseManager<T> {

    @PersistenceContext(name = "DataSourceEx")
    protected EntityManager em;
    protected final Class<T> tClass;
    private final String entityTable;
    protected String searchField;
    protected String carLinkFieldName;

    public BaseManager(Class<T> cls) {
        this.tClass = cls;
        String table = "";
        if (tClass.getAnnotation(Table.class) != null) {
            Table tAnn = tClass.getAnnotation(Table.class);
            table = tAnn.name();
        }
        entityTable = table.isEmpty() ? tClass.getSimpleName() : table;
    }

    public T get(Long id) {
        return em.find(tClass, id);
    }

    public List<T> getAll() throws Exception {
        return getSortedList("", null, null, null);
    }

    public T update(T object) {
        return em.merge(object);
    }

    public abstract T convert(Map<String, String> params) throws PartIsAlreadyUsedException, PartNotFoundException;

    public List<T> getSortedList(String sortExpression, Integer pageSize, Integer page, String filter) throws Exception {
        SortValidation sortValidation = validateSortParams(sortExpression);
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(tClass);
        Root<T> root = criteriaQuery.from(tClass);
        if (sortValidation.sort.equals("asc")) {
            criteriaQuery.orderBy(criteriaBuilder.asc(root.get(sortValidation.fieldName)));
        } else {
            criteriaQuery.orderBy(criteriaBuilder.desc(root.get(sortValidation.fieldName)));
        }
        if (filter != null && !filter.isEmpty()) {
            criteriaQuery.select(root).where(criteriaBuilder.like(root.get(searchField), "%" + filter + "%"));
        }
        TypedQuery<T> tq = em.createQuery(criteriaQuery);
        if (pageSize != null && page != null) {
            tq.setFirstResult((page - 1) * pageSize).setMaxResults(pageSize);
        }
        return tq.getResultList();
    }

    /**
     * Not for car
     *
     * @return
     */
    public List<T> getFreeItems() {
        String request = String.format("select * from %s where id not in (select c.%s from Car c)", entityTable, carLinkFieldName);
        return em.createNativeQuery(request, tClass).getResultList();
    }

    public int count() {
        String request = "select count(*) from " + entityTable;
        return ((Number) em.createNativeQuery(request).getSingleResult()).intValue();
    }

    protected SortValidation validateSortParams(String paramsStr) throws Exception {
        if (paramsStr == null || paramsStr.isEmpty()) {
            return new SortValidation("asc", "id");
        }
        String[] params = paramsStr.split(",");
        if (params.length < 2) {
            throw new Exception("Invalid sorting expression");
        }
        String p2 = params[1];
        if (!Arrays.asList("asc", "desc").contains(p2.toLowerCase().trim())) {
            throw new Exception("Invalid sorting expression");
        } else {
            return new SortValidation(params[1].trim(), params[0]);
        }
    }

    @Transactional
    public String remove(Long id) throws Exception {
        Long carId = linkToCar(id);
        if (carId != null) {
            throw new Exception("Unable to delete: part [" + id + "] is in car [" + carId + "]");
        }
        T objToRemove = em.find(tClass, id);
        if (objToRemove == null) {
            throw new UnknownPartException(tClass.getName() + " [" + id + "] not found");
        } else {
            em.remove(objToRemove);
            return "Deleted";
        }
    }

    private Long linkToCar(Long id) {
        String request = "select id from car where " + carLinkFieldName + " = " + id;
        List result = em.createNativeQuery(request).getResultList();
        return !result.isEmpty() ? ((Number) result.get(0)).longValue() : null;
    }

    protected static class SortValidation {
        public final String sort;
        public final String fieldName;

        public SortValidation(String sort, String fieldName) {
            this.sort = sort;
            this.fieldName = fieldName;
        }
    }
}
