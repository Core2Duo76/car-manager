package c2d.empl.entity;

import javax.persistence.*;

@Entity()
@Table(name = "transmission")
public class Transmission {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transmission_seq")
    @SequenceGenerator(name = "transmission_seq", sequenceName = "transmission_seq")
    private Long id;
    private String serial;
    @Enumerated(EnumType.ORDINAL)
    private TransmissionType typeId;

    public Transmission() {
    }

    public Transmission(Long id, String serial, TransmissionType typeId) {
        this.id = id;
        this.serial = serial;
        this.typeId = typeId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public TransmissionType getTypeId() {
        return typeId;
    }

    public void setTypeId(TransmissionType typeId) {
        this.typeId = typeId;
    }

    @Override
    public String toString() {
        return typeId.toString();
    }
}
