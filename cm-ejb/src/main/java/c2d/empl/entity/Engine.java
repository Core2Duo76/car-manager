/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c2d.empl.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ENGINE")
public class Engine implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENGINE_SEQ")
    @SequenceGenerator(name = "ENGINE_SEQ", sequenceName = "ENGINE_SEQ")
    private Long id;

    @Column(name = "name", length = 1024)
    private String name;
    private String serial;
    private Double power;
    private Long capacity;
    @Enumerated(EnumType.ORDINAL)
    private EngineType type;

    public Engine() {
    }

    public Engine(Long id, String name, String serial, Double power, Long capacity, EngineType type) {
        this.id = id;
        this.name = name;
        this.serial = serial;
        this.power = power;
        this.capacity = capacity;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public Double getPower() {
        return power;
    }

    public void setPower(Double power) {
        this.power = power;
    }

    public Long getCapacity() {
        return capacity;
    }

    public void setCapacity(Long capacity) {
        this.capacity = capacity;
    }

    public EngineType getType() {
        return type;
    }

    public void setType(EngineType type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return power + " kw/" + capacity + "/" + type;
    }

}
