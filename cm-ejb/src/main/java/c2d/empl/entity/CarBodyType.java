package c2d.empl.entity;

public enum CarBodyType {

    SEDAN(1),
    HACTHBACK(2),
    COUPE(3),
    UNI(4);

    private final int id;

    CarBodyType(int id) {
        this.id = id;
    }

    public int id() {
        return id;
    }
}
