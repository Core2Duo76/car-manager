package c2d.empl.entity;

public enum TransmissionType {

    AUTO(1),
    DCT(2),
    DSG(3),
    TIPTRONIC(4),
    CVT(5);

    private final int id;

    TransmissionType(int id) {
        this.id = id;
    }


}
