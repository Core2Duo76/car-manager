package c2d.empl.entity;

import javax.persistence.*;

@Entity
@Table(name = "body")
public class CarBody {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BODY_SEQ")
    @SequenceGenerator(name = "BODY_SEQ", sequenceName = "BODY_SEQ")
    private Long id;
    @Enumerated(EnumType.ORDINAL)
    private CarBodyType typeId;
    private String color;
    private Integer doors;
    private String vin;

    public CarBody() {
    }

    public CarBody(Long id, CarBodyType typeId, String color, Integer doors, String vin) {
        this.id = id;
        this.typeId = typeId;
        this.color = color;
        this.doors = doors;
        this.vin = vin;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CarBodyType getTypeId() {
        return typeId;
    }

    public void setTypeId(CarBodyType typeId) {
        this.typeId = typeId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getDoors() {
        return doors;
    }

    public void setDoors(Integer doors) {
        this.doors = doors;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    @Override
    public String toString() {
        return color + "/" + doors + " d.";
    }
}
