package c2d.empl.entity;

public enum EngineType {

    ROTARY(1),
    V(2),
    BOXER(3),
    STRAIGHT(4);

    private final int id;

    EngineType(int i) {
        this.id = i;
    }

    public int id() {
        return id;
    }

}
