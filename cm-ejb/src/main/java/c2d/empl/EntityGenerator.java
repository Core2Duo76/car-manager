package c2d.empl;

import c2d.empl.entity.*;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Stateful
public class EntityGenerator {

    @PersistenceContext(name = "DataSourceEx")
    EntityManager em;
    /**
     * Decision to generate
     */
    private boolean mustGenerate = true;

    private final List<String> hbColor = Arrays.asList("red", "black", "green", "yellow", "white");
    private final List<Integer> hbDoors = Arrays.asList(3, 5);

    /**
     * Generates entities for empty database
     */
    @Transactional
    public void deployEntities() {
        if (!mustGenerate) return;
        int index = 1;
        Random random = new Random();
        for (EngineType engineType : EngineType.values()) {
            em.merge(new Engine(null,
                    "Engine " + index++,
                    UUID.randomUUID().toString(),
                    BigDecimal.valueOf(100d + random.nextDouble() * 100d).setScale(1, RoundingMode.FLOOR).doubleValue(),
                    1000 + (long) 100 * random.nextInt(10),
                    engineType));
        }
        for (CarBodyType carBodyType : CarBodyType.values()) {
            CarBody body = new CarBody(null, carBodyType,
                    hbColor.get(random.nextInt(hbColor.size())),
                    hbDoors.get(random.nextInt(2)),
                    UUID.randomUUID().toString());
            em.merge(body);
        }
        for (TransmissionType transmissionType : TransmissionType.values()) {
            Transmission transmission = new Transmission(null, UUID.randomUUID().toString(), transmissionType);
            em.merge(transmission);
        }
        mustGenerate = false;
    }

}
