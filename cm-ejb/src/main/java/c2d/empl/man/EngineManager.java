package c2d.empl.man;

import c2d.empl.BaseManager;
import c2d.empl.entity.Engine;
import c2d.empl.entity.EngineType;
import c2d.empl.ex.DuplicatePartException;

import javax.ejb.Stateless;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

@Stateless
public class EngineManager extends BaseManager<Engine> {

    public EngineManager() {
        super(Engine.class);
        this.searchField = "serial";
        this.carLinkFieldName = "engineId";
    }

    public Engine create(Engine in) throws DuplicatePartException {
        in.setId(null);
        String serial = in.getSerial();
        if (serial == null || serial.isEmpty()) {
            in.setSerial(UUID.randomUUID().toString());
        } else {
            List storedEngines = em.createNativeQuery("select * from engine where serial = :serial", Engine.class)
                    .setParameter("serial", serial)
                    .setMaxResults(1)
                    .getResultList();
            if (!storedEngines.isEmpty()) {
                throw new DuplicatePartException("Serial number " + serial + " is already exists");
            }
        }
        em.persist(in);
        return in;
    }

    @Override
    public Engine convert(Map<String, String> params) {
        String serial = params.get("serial");
        Long id = params.get("id") != null ? Long.parseLong(params.get("id")) : null;
        String sType = params.get("type");
        String name = params.getOrDefault("name", "Engine " + new Random().nextInt(1000)).replaceAll("\\+", " ");
        EngineType eType = EngineType.valueOf(sType.toUpperCase());
        Double power = Double.parseDouble(params.get("power"));
        Long capacity = ((long) Double.parseDouble(params.get("capacity")));
        return new Engine(id, name, serial, power, capacity, eType);
    }
}
