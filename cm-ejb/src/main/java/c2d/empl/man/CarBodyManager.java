package c2d.empl.man;

import c2d.empl.BaseManager;
import c2d.empl.entity.CarBody;
import c2d.empl.entity.CarBodyType;
import c2d.empl.ex.DuplicatePartException;

import javax.ejb.Stateless;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Stateless
public class CarBodyManager extends BaseManager<CarBody> {

    public CarBodyManager() {
        super(CarBody.class);
        this.searchField = "vin";
        this.carLinkFieldName = "bodyId";
    }

    public CarBody create(CarBody in) throws DuplicatePartException {
        in.setId(null);
        if (in.getVin() == null || in.getVin().isEmpty()) {
            in.setVin(UUID.randomUUID().toString());
        } else {
            String vin = in.getVin();
            List vins = em.createNativeQuery("select * from body where vin = :vin")
                    .setParameter("vin", vin)
                    .setMaxResults(1)
                    .getResultList();
            if (!vins.isEmpty()) {
                throw new DuplicatePartException("VIN '" + vin + "' already exists");
            }
        }
        em.persist(in);
        return in;
    }

    @Override
    public CarBody convert(Map<String, String> params) {
        String color = params.getOrDefault("color", "no color").replaceAll("\\+", " ");
        Long id = params.get("id") != null ? Long.parseLong(params.get("id")) : null;
        int doors = Integer.parseInt(params.get("doors"));
        String vin = UUID.randomUUID().toString();
        CarBodyType type = CarBodyType.valueOf(params.get("typeId"));
        return new CarBody(id, type, color, doors, vin);
    }
}
