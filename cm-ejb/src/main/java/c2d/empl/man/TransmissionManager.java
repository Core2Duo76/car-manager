package c2d.empl.man;

import c2d.empl.BaseManager;
import c2d.empl.entity.Transmission;
import c2d.empl.entity.TransmissionType;
import c2d.empl.ex.DuplicatePartException;

import javax.ejb.Stateless;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Stateless
public class TransmissionManager extends BaseManager<Transmission> {

    public TransmissionManager() {
        super(Transmission.class);
        this.searchField = "serial";
        this.carLinkFieldName = "transmissionId";
    }

    public Transmission create(Transmission in) throws DuplicatePartException {
        in.setId(null);
        String serial = in.getSerial();
        if (serial == null || serial.isEmpty()) {
            in.setSerial(UUID.randomUUID().toString());
        } else {
            List list = em.createNativeQuery("select * from transmission where serial = :serial", Transmission.class)
                    .setParameter("serial", serial).setMaxResults(1).getResultList();
            if (!list.isEmpty()) {
                throw new DuplicatePartException("Serial '" + serial + "' already exists");
            }
        }
        em.persist(in);
        return in;
    }

    @Override
    public Transmission convert(Map<String, String> params) {
        TransmissionType type = TransmissionType.valueOf(params.get("typeId"));
        Long id = params.get("id") != null ? Long.parseLong(params.get("id")) : null;
        String serial = UUID.randomUUID().toString();
        return new Transmission(id, serial, type);
    }
}
