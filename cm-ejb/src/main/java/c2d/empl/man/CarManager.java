package c2d.empl.man;

import c2d.empl.BaseManager;
import c2d.empl.entity.Car;
import c2d.empl.entity.CarBody;
import c2d.empl.entity.Engine;
import c2d.empl.entity.Transmission;
import c2d.empl.ex.PartIsAlreadyUsedException;
import c2d.empl.ex.PartNotFoundException;

import javax.ejb.Stateless;
import java.util.List;
import java.util.Map;

@Stateless
public class CarManager extends BaseManager<Car> {

    public CarManager() {
        super(Car.class);
    }

    public Car build(Long engineId, Long trId, Long bodyId, String name) throws PartNotFoundException, PartIsAlreadyUsedException {
        Engine engine = checkPart(Engine.class, engineId, "engineId");
        Transmission transmission = checkPart(Transmission.class, trId, "bodyId");
        CarBody body = checkPart(CarBody.class, bodyId, "bodyId");
        Car car = buildFromParts(engine, transmission, body);
        car.setName(name);
        return em.merge(car);
    }

    <T> T checkPart(Class<T> cls, long id, String paramName) throws PartNotFoundException, PartIsAlreadyUsedException {
        T part = em.find(cls, id);
        if (part == null) {
            throw new PartNotFoundException(cls);
        }
        long isUsed = checkPartIsAlreadyUsed(paramName, id);
        if (isUsed > 0) {
            throw new PartIsAlreadyUsedException(paramName + " [" + id + "] is already in used in Car [" + isUsed + "]");
        }
        return part;
    }

    Car buildFromParts(Engine engine, Transmission transmission, CarBody carBody) {
        Car car = new Car();
        car.setTransmission(transmission);
        car.setBody(carBody);
        car.setEngine(engine);
        return car;
    }

    long checkPartIsAlreadyUsed(String paramName, long id) {
        String SEARCH = "select c from Car c where %s = :%s";
        String conditions = String.format(SEARCH, paramName, paramName);
        List<Car> list = em.createQuery(conditions, Car.class)
                .setParameter(paramName, id)
                .setMaxResults(1)
                .getResultList();
        return !list.isEmpty() ? list.get(0).getId() : 0;
    }

    @Override
    public Car convert(Map<String, String> params) throws PartIsAlreadyUsedException, PartNotFoundException {
        Long trId = Long.parseLong(params.get("transmission"));
        Long bodyId = Long.parseLong(params.get("body"));
        Long engineId = Long.parseLong(params.get("engine"));
        String name = params.getOrDefault("name", "Default name").replaceAll("\\+", " ");
        return build(engineId, trId, bodyId, name);
    }
}

