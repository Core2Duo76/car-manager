package c2d.empl.ex;

public class DuplicatePartException extends Exception {

    public DuplicatePartException(String message) {
        super(message);
    }
}
