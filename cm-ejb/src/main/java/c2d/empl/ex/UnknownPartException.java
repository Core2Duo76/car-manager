package c2d.empl.ex;

public class UnknownPartException extends Exception {

    public UnknownPartException(String message) {
        super(message);
    }
}
