package c2d.empl.ex;

public class PartIsAlreadyUsedException extends Exception {

    public PartIsAlreadyUsedException(String message) {
        super(message);
    }
}
