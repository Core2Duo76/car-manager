package c2d.empl.ex;

public class PartNotFoundException extends Exception {

    public PartNotFoundException(String message) {
        super(message);
    }

    public PartNotFoundException(Class item) {
        super("Part of  type [" + item.getSimpleName() + "] not found");
    }
}
